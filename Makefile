CC=g++
.SILENT:
linux64: adventureGame.cpp items.hpp characters.hpp areas.hpp
	echo "Building for Linux (64 bit)..."
	[ -d "bin" ] || mkdir bin
	g++ -m64 -o bin/adventureGame64 adventureGame.cpp
	echo "Done"
linux: adventureGame.cpp items.hpp characters.hpp areas.hpp
	echo "Building for Linux (32 bit)..."
	[ -d "bin" ] || mkdir bin
	g++ -m32 -o bin/adventureGame adventureGame.cpp
	echo "Done"
windows: adventureGame.cpp items.hpp characters.hpp areas.hpp
	echo "Building for Windows (32 bit)..."
	[ -d "bin" ] || mkdir bin
	i686-w64-mingw32-g++ --static ./adventureGame.cpp -o bin/adventureGame.exe
	echo "Done"
windows64: adventureGame.cpp items.hpp characters.hpp areas.hpp
	echo "Building for Windows (64 bit)..."
	[ -d "bin" ] || mkdir bin
	x86_64-w64-mingw32-g++ --static ./adventureGame.cpp -o bin/adventureGame64.exe
	echo "Done"
.PHONY : all
all: adventureGame.cpp items.hpp characters.hpp areas.hpp
	make linux64
	#make linux
	make windows64
	make windows