area nowhere;
nowhere.description = "You see no path in that direction.";
nowhere.shortDescription = "You see no path in that direction.";
nowhere.invalid = true;
nowhere.function = &fixPosition;
area startingPoint;
startingPoint.description = "You find yourself in a basement, you're not sure how you got here. You see a Japanese thing and have an unreasonable urge to destroy it. You see  You see a tunnel to the north, the exit to the south, and infinite recursion to the west.";
startingPoint.shortDescription = "Basement";
startingPoint.items.push_back(tickler);
startingPoint.enemies.push_back(japaneseThing);
area bathroom;
bathroom.description = "You're in a bathroom, the floor is concerningly wet. You see a door to the south.";
bathroom.shortDescription = "Bathroom";
area exit;
exit.description = "You decide to leave the game. Goodbye!";
exit.shortDescription = "You decide to leave the game. Goodbye!";
exit.function = &leave;


